from datetime import datetime
from airflow import Dataset

from airflow import DAG
from airflow.decorators import task
from airflow.operators.bash import BashOperator

# A DAG represents a workflow, a collection of tasks
# https://airflow.apache.org/docs/apache-airflow/stable/index.html 

with DAG(dag_id="hello-airflow", start_date=datetime(2022, 1, 1), schedule="0 0 * * *", tags=["example"]) as dag:

    # Tasks are represented as operators
    hello = BashOperator(task_id="hello", bash_command="echo hello")

    @task()
    def airflow():
        print("airflow")

    # Set dependencies between tasks
    hello >> airflow()